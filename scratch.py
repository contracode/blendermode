#!/usr/bin/env python
# -=- coding: utf-8 -*-
import os
import re
import sys
import subprocess
from . import utils

reserved = [
    "<Alt>space",
    "<Primary><Alt>KP_0"
]

commands = [
    "gsettings list-recursively org.gnome.desktop.wm.keybindings",
    "gsettings list-recursively org.gnome.desktop.wm.preferences | grep 'mouse-button-modifier'"]

settings = list()
try:
    # Run each command and store all settings.
    for command in commands:
        output = subprocess.check_output(
            ["/bin/bash", "-c", command]).decode("utf-8")
        settings.extend(output.split(os.linesep))
except (subprocess.CalledProcessError) as error:
    # Exit if the command failed.
    print(error)
    sys.exit()
else:
    # Filter out blank lines from the command output.
    settings = list(filter(None, settings))

# Parse settings.
setting_pattern = r"""
    ([a-z.]+)\          # Match GSettings schema.
    ([a-z0-9-]+)\       # Match GSettings key.
    (?!@as|\[?''\]?)    # Don't match disabled (i.e., blank) settings.
    ([\w_<>', \[\]]+)   # Match values.
    """
setting_regex = re.compile(setting_pattern, re.VERBOSE)

for setting in settings:
    match = re.search(setting_regex, setting)
    if match:
        print(match.groups(3))

